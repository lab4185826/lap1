using UnityEngine;

namespace kassakarn.GameDev3.Chapter1
{
    public class ItemTypeComponent : MonoBehaviour
    {
        // Start is called before the first frame update
        
        [SerializeField]
        protected ItemType m_ItemType;

        public ItemType Type
        {
            get { return m_ItemType; }
            set { m_ItemType = value; }
        }

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
